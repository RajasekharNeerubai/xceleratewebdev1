export class Description{
    descriptionId: number;
    description: string;
    updatedBy: string;
    updatedDatetime: Date;
}