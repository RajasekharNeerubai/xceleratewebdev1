export class NotesInfo {
    datetime: Date;
    note:string;
    jobType:string;
    noteby:string;
    jobImage:string;
    category:string;
    jobHashCode: string;
  }