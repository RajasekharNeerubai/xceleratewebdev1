export interface PersonBasicInfo {
    id: number;
    firstName: string;
    lastName: string;
    phoneNo: string;
}