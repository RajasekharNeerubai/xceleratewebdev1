export class WarmleadsList{
    masterjobno:string;
    name:string;
    leadtype:string;
    address:string;
    phonenumber:string;
    fax:string;
    billingaddress:string;
    notes:string;
    assignto:string;
}