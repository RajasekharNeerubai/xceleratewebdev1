export interface GoogleMapMarker {
	lat: number;
	lng: number;
	label?: string;
	draggable: boolean;
}