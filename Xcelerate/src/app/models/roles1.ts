export class RolesInfo {
    id: number;
    name:string;
    phoneNo:string;
    firstName: string;
    lastName: string;
    role: string;
    email:string;
  }