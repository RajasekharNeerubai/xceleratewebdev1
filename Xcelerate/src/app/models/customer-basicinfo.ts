export class CustomerBasicInfo {
    customerId:number;
    customerName:string;
    firstName:string;
    lastName:string;
    email: string;
    address1: string;
    address2: string;
    city:string;
    state:String;
    zip: String;
    phone1:string;
    mobile:string;
    workPhone:string;
  }

     