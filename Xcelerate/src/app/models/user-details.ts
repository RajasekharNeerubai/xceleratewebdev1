export class UserDetails {
   userId:number;
   createdBy:string;
   createdDatetime:string;
   emailId:string;
   firstName:string;
   inactiveReason:string;
   lastName:string;
   mobileNo:string;
   phoneNo:string;
   profilePic:string;
   status:string;
   userName:string;
}