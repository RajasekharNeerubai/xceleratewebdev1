export class JobImagesList{
    jobActiveImage:string;
    jobInactiveImage:string;
    imageActive:boolean;
    jobName:string;
}