export class CompaniesList{
    name:string;
    address:string;
    phoneNo:string;
    fax:string;
    billingaddress:string;
    addemp:string;
    salesrep:string;
}