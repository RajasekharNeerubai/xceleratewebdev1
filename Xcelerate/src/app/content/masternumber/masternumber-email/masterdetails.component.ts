import { Component, ElementRef, Input } from '@angular/core';
import { MasterNumberData } from '../../../models/masternumber-data';
import { InsuranceDetails } from '../../../models/insurance-details';
import { Description } from '../../../models/description';
import { ContactDetails } from '../../../models/contact-details';
import { SubJobsGrid } from '../../../models/subjobs-grid';
import { NotesInfo } from '../../../models/notes';

@Component({
  selector: 'masterjob-details',
  templateUrl: './masterdetails.component.html',
})
export class MasterJobDetails {

  @Input() masterNumberData: MasterNumberData;
  @Input() insurances: InsuranceDetails;
  @Input() description: Description[];
  @Input() contacts: ContactDetails[];
  @Input() subJobs: SubJobsGrid[];
  @Input() notes: NotesInfo[];
  elRef: ElementRef;
  
  constructor(elRef: ElementRef) {
      this.elRef = elRef;
  }
}