import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';

import { TitleTextService } from '../../../shared/services/titletext.service';
import { APIUrls } from '../../../shared/constants/apiurls';
import { TokenValue } from '../../../shared/services/httpcall/token.constant';
import { Messages } from '../../../shared/constants/messages';
import { MasterNumberData } from '../../../models/masternumber-data';
import { NotesInfo } from '../../../models/notes';

import {SubJobsGrid} from '../../../models/subjobs-grid';
import {InsuranceDetails} from '../../../models/insurance-details';
import {ContactDetails} from '../../../models/contact-details';
import {Description} from '../../../models/description';
import {FilesPath} from '../../../shared/constants/filespath';
import {GrowlModule,Message} from 'primeng/primeng';
import { DocumentsInfo } from '../../../models/documents';
import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-masternumberdetails',
  templateUrl: './masternumber-details.component.html',
  styleUrls: ['./masternumber-details.component.css'],
  providers: [DatePipe]
})
export class MasterNumberDetailsComponent implements OnInit {
    dtOptions: DataTables.Settings = {
      
    };
    sub:any;
    id:string;
    url:string;
    myData:any;
    isError:boolean;
    serverErrorMessage:string;
    masterNumberData: MasterNumberData;
    parameterID:string;
    cols: any[];
    jobImagepath:string = FilesPath.JOBTYPE_ICONS ;
    subJobs: SubJobsGrid[];
    insurances: InsuranceDetails;
    contacts: ContactDetails[];
    notes: NotesInfo[];
    refSource: string;
    description: Description[];
    pictures: DocumentsInfo[];
 
    display: boolean = false;
    msgs: Message[] = [];
    mastNumData: any;
    downloadPdf: boolean = false;
    displayEmail: boolean = false;
    emailTo: string;
    emailSubject: string;
    emailSource: string='MasterDetails';
    emailOption: string='Master Details';
    emailPattern =/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9\-]+\.)+([a-zA-Z0-9\-\.]+)+([;]([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9\-]+\.)+([a-zA-Z0-9\-\.]+))*$/;
    showMailToMsg: boolean = false;
    showSubjectMsg: boolean = false;
    showDialog() {
      this.display = true;
  }

  showEmail(){
    this.emailTo = "";
    this.showMailToMsg = false;
    this.showSubjectMsg = false;
    this.emailSubject = this.masterNumberData.mastNumCode +" Details";
    this.displayEmail = true;
  }
  scroll(el) {
    //alert('scroll');
    el.scrollIntoView();
}
  
 constructor(private data: TitleTextService, private route: ActivatedRoute, private http: HttpClient,private datePipe: DatePipe) {
   this.cols = [
                { field: 'status', header: 'Current Status', showData:true},
                { field: 'subJobCode', header: 'File Number',showData:true },
                { field: 'jobImage', header: 'File Details',showData:true},
                { field: 'supervisor', header: 'Supervisor', showData:true},
                { field: 'estimator', header: 'Estimator', showData:true},
            ];
 }

    getMasterNumberDetails(){
      let getParams = new HttpParams().set('mastNumHash', this.parameterID);
      this.url =APIUrls.hosturl+APIUrls.MasterNumberDetails
      this.http.get(this.url,{params:getParams})
      .subscribe(data=>{
          this.myData = data;
          this.masterNumberData=this.myData.masterNumberDetails;
          this.data.changeMessage("Master Job Number: "+"   "+this.masterNumberData.mastNumCode);
      },
      (err: HttpErrorResponse) => {
          this.isError=true;
          this.serverErrorMessage = Messages.ServerErrorMessage;
            if (err.error instanceof Error) {
              console.log("Client-side error occured.");
            } else {
              console.log("Server-side error occured.");
            }
          }
      );
    }

    getMasterNumberDescription(){
      let getParams = new HttpParams().set('mastNumHash', this.parameterID);
      this.url =APIUrls.hosturl+APIUrls.MasterNumberDescription
      this.http.get(this.url,{params:getParams})
      .subscribe(data=>{
          this.myData = data;
          this.description=this.myData.descriptionDetails;
      },
      (err: HttpErrorResponse) => {
          this.isError=true;
          this.serverErrorMessage = Messages.ServerErrorMessage;
            if (err.error instanceof Error) {
              console.log("Client-side error occured.");
            } else {
              console.log("Server-side error occured.");
            }
          }
      );
    }

    getSubJobs(){
        let getParams = new HttpParams().set('mastNumHash', this.parameterID);
        this.url =APIUrls.hosturl+APIUrls.MasterNumberJobs;
        this.http.get(this.url,{params:getParams})
        .subscribe(data=>{
        this.myData=data;
        this.subJobs=this.myData.jobs;
        //console.log('this count'+this.subJobs.length);
        for (let file of this.subJobs) {
                file.style=file.status.toLowerCase().replace(/ /g,"-");
                file.jobImage=this.jobImagepath+file.jobType+".png";
        }
        },
        (err: HttpErrorResponse) => {
                this.isError=true;
                this.serverErrorMessage = Messages.ServerErrorMessage;
                if (err.error instanceof Error) {
                        console.log("Client-side error occured.");
                } else {
                        console.log("Server-side error occured.");
                }
        }
        );
    }
    getInsuranceDetails(){
      const params = new HttpParams().set('mastNumHash',this.parameterID);
      this.url =APIUrls.hosturl+APIUrls.MasterNumberInsuranceDetails;
      this.http.get(this.url,{params:params})
        .subscribe(data=>{
          this.myData=data;
          this.insurances=this.myData.insuranceDetails;
        },
        (err: HttpErrorResponse) => {
            this.isError=true;
            this.serverErrorMessage = Messages.ServerErrorMessage;
            if (err.error instanceof Error) {
              console.log("Client-side error occured.");
            } else {
              console.log("Server-side error occured.");
            }
          }
        );
    }

    getNotes(){
      const params = new HttpParams().set('mastNumHash',this.parameterID);
      this.url =APIUrls.hosturl+APIUrls.MasterNumberNotes;
      this.http.get(this.url,{params:params})
          .subscribe(data=>{
            this.myData=data;
            this.notes=this.myData.notes;
             for (let note of this.notes) {
                note.jobImage=this.jobImagepath+note.jobType+".png";
            }
            console.log(this.notes);
          },
          (err: HttpErrorResponse) => {
              this.isError=true;
              this.serverErrorMessage = Messages.ServerErrorMessage;
              if (err.error instanceof Error) {
                console.log("Client-side error occured.");
              } else {
                console.log("Server-side error occured.");
              }
            }
          );
    }

    getContacts() {
      const params = new HttpParams().set('mastNumHash',this.parameterID);
      this.url =APIUrls.hosturl+APIUrls.MasterNumberContactDetails
      this.http.get(this.url,{params:params})
          .subscribe(data=>{
            this.myData=data;
            this.contacts=this.myData.contacts;
            this.refSource=this.myData.referredSource;
          },
          (err: HttpErrorResponse) => {
              this.isError=true;
              this.serverErrorMessage = Messages.ServerErrorMessage;
              if (err.error instanceof Error) {
                console.log("Client-side error occured.");
              } else {
                console.log("Server-side error occured.");
              }
            }
          );
   }
    ngOnInit() {
      this.dtOptions = {
        pagingType: 'full_numbers',
        order:[],
      }
      this.sub = this.route.params.subscribe(params => {
      this.id = params['id']}); 
      this.parameterID=this.id;
      this.getMasterNumberDetails();
      this.getSubJobs();
      this.getInsuranceDetails();
      this.getContacts();
      this.getMasterNumberDescription();
      this.getNotes();
      this.getPictures();
    }

    updateDescription($event){
      this.mastNumData={};
      this.mastNumData.mastNumId=this.masterNumberData.mastNumId;
      this.mastNumData.description=$event.description;
      this.url=APIUrls.hosturl+APIUrls.SaveMasterNumberDescription;
      this.http.post(this.url, this.mastNumData)
        .subscribe(data=>{
          this.myData=data;
          //this.showSuccess(this.myData.message);
          this.getMasterNumberDescription();
        },
        (err: HttpErrorResponse) => {
            this.isError=true;
            this.serverErrorMessage = Messages.ServerErrorMessage;
            if (err.error instanceof Error) {
              console.log("Client-side error occured.",err);
            } else {
              console.log("Server-side error occured.",err);
            }
          }
        );
     }

     updateContacts($event){
       console.log($event.referredSource);
       console.log($event.contacts);
        this.mastNumData={};
        this.mastNumData.mastNumId=this.masterNumberData.mastNumId;
        this.mastNumData.referredSource=$event.referredSource;
        this.mastNumData.contacts=$event.contacts;
        console.log(this.mastNumData);
        this.url=APIUrls.hosturl+APIUrls.UpdateMasterNumberContacts;
        this.http.post(this.url, this.mastNumData)
          .subscribe(data=>{
          this.myData=data;
          this.showSuccess(this.myData.message);
          this.getContacts();
        },
        (err: HttpErrorResponse) => {
            this.isError=true;
            this.serverErrorMessage = Messages.ServerErrorMessage;
            if (err.error instanceof Error) {
              console.log("Client-side error occured.",err);
            } else {
              console.log("Server-side error occured.",err);
            }
          }
        );
     }

    updateMasterNumberData($event){
      this.mastNumData=$event.masterNumberData;
        this.mastNumData.mastNumId=this.masterNumberData.mastNumId;
        this.url=APIUrls.hosturl+APIUrls.UpdateMasterNumberDetails;
        console.log(this.mastNumData);
        this.http.post(this.url, this.mastNumData)
          .subscribe(data=>{
          this.myData=data;
          this.showSuccess(this.myData.message);
          this.getMasterNumberDetails();
          this.getInsuranceDetails();
        },
        (err: HttpErrorResponse) => {
            this.isError=true;
            this.serverErrorMessage = Messages.ServerErrorMessage;
            if (err.error instanceof Error) {
              console.log("Client-side error occured.",err);
            } else {
              console.log("Server-side error occured.",err);
            }
          }
        );
    }

    showSuccess(message) {
        this.msgs = [];
        this.msgs.push({severity:'success', summary:'Success Message', detail:message});
    }

    getPictures(){
        let getParams = new HttpParams().set('mastNumHash', this.parameterID);
        this.url =APIUrls.hosturl+APIUrls.MasterNumberPictures;
        this.http.get(this.url,{params:getParams})
        .subscribe(data=>{
            this.myData = data;
            this.pictures=this.myData.pictures;
        },
        (err: HttpErrorResponse) => {
            this.isError=true;
            this.serverErrorMessage = Messages.ServerErrorMessage;
              if (err.error instanceof Error) {
                console.log("Client-side error occured.");
              } else {
                console.log("Server-side error occured.");
              }
            }
        );
    }

    download(fileName){
      this.downloadPdf=true;
      setTimeout(()=>{
      var element = document.getElementById('masterJobDetails');
      element.style.display = 'block';
  
      html2canvas(element).then(function(canvas) {
      var img = canvas.toDataURL("image/png");
      var doc = new jsPDF();
      doc.addImage(img,'JPEG',2, 2, 205, 190);
      doc.save(fileName+'.pdf');
     // element.style.display = 'none';
      });
      this.downloadPdf=false;
      },1000);
    }

    sendMail(recipients){
      let email: any;
      console.log(recipients.errors);
      if(recipients.errors==null && this.emailTo!=undefined && this.emailTo!="" && this.emailSubject!=undefined && this.emailSubject!=""){
        email={};
        // var element = document.getElementById('emailBody');
        // html2canvas(element).then(function(canvas) {
        //   email.message=canvas.toDataURL("string");
        // });
        let content: string='';
        content+='<html><head></head><body style="padding: 0px;margin: 0px;font-family: Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol;"><div class="em_pdf" style="width:98%;padding:0px;margin:10px 1%;float:left;overflow:hidden;-moz-box-shadow: 0 0 5px #888;-webkit-box-shadow: 0 0 5px#888;box-shadow: 0 0 5px #888;">'+
                 '<div class="em_title" style="width:100%;padding:5px 10px;margin:0px;float:left;font-size:16px;letter-spacing:0.5px;background:#595959;color:#FFF;box-shadow: 0 5px 5px -5px #000000;">Master Details</div><div class="em_pdf_info" style="width: 98.5%;float: left;padding:10px;margin: 0px;overflow: hidden;"><div class="em_row" style="width: 100%;padding: 0px;margin:5px 0px;float: left;overflow: hidden;">'+
                 '<div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;"><div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">Comapany Name</div><div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';
        if(this.masterNumberData!=null && this.masterNumberData!=undefined && this.masterNumberData.companyName!=null){
            content+=this.masterNumberData.companyName;
        }
        content+='</div></div><div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;">'+
                  '<div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">'+
                  'Owner Name</div><div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';
        if(this.masterNumberData!=null && this.masterNumberData!=undefined && this.masterNumberData.customerName!=null){
            content+=this.masterNumberData.customerName;
        }
        content+='</div></div><div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;">'+
                 '<div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">'+
                 'Responsible Party</div><div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';
        if(this.masterNumberData!=null && this.masterNumberData!=undefined && this.masterNumberData.responsibleParty!=null){
            content+=this.masterNumberData.responsibleParty;
        }  
        content+='</div></div><div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;">'+
                 '<div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">'+
                 'Email</div><div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';
        if(this.masterNumberData!=null && this.masterNumberData!=undefined && this.masterNumberData.email!=null){
            content+=this.masterNumberData.email;
        }  
        content+='</div></div></div><div class="em_row" style="width: 100%;padding: 0px;margin:5px 0px;float: left;overflow: hidden;">'+
                '<div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;">'+
                '<div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">'+
                'Phone No.</div><div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';  
        if(this.masterNumberData!=null && this.masterNumberData!=undefined && this.masterNumberData.phoneNo!=null){
            content+=this.masterNumberData.phoneNo;
        }   
        content+='</div></div><div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;">'+
                 '<div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">'+
                 'Cell No.</div><div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';
        if(this.masterNumberData!=null && this.masterNumberData!=undefined && this.masterNumberData.mobile!=null){
            content+=this.masterNumberData.mobile;
        }
        content+='</div></div><div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;">'+
                 '<div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">'+
                 'Work No.</div><div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';         
        if(this.masterNumberData!=null && this.masterNumberData!=undefined && this.masterNumberData.workPhone!=null){
            content+=this.masterNumberData.workPhone;
        }
        content+='</div></div><div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;">'+
                '<div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">'+
                'Tenant Name</div><div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';
        if(this.masterNumberData!=null && this.masterNumberData!=undefined && this.masterNumberData.tenantName!=null){
            content+=this.masterNumberData.tenantName;
        }
        content+='</div></div></div><div class="em_row" style="width: 100%;padding: 0px;margin:5px 0px;float: left;overflow: hidden;">'+
                 '<div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;">'+
                 '<div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">'+
                 'Tenant No.</div><div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';
        if(this.masterNumberData!=null && this.masterNumberData!=undefined && this.masterNumberData.tenantNumber!=null){
            content+=this.masterNumberData.tenantNumber;
        }  
        content+='</div></div><div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;">'+
                 '<div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">Loss Address</div>'+
                 '<div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';
        if( this.masterNumberData !== null && this.masterNumberData !== undefined && this.masterNumberData.lossAddress !== null){
           content+=this.masterNumberData.lossAddress;
        }
        if( this.masterNumberData !== null && this.masterNumberData !== undefined && this.masterNumberData.lossAddressCity !== null){
           content+=" "+this.masterNumberData.lossAddressCity;
        }
        if( this.masterNumberData !== null && this.masterNumberData !== undefined && this.masterNumberData.lossAddressState !== null){
           content+=" "+this.masterNumberData.lossAddressState;
        }
        if( this.masterNumberData !== null && this.masterNumberData !== undefined && this.masterNumberData.lossAddressZip !== null){
           content+=" "+this.masterNumberData.lossAddressZip;
        }
        content+='</div></div><div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;">'+
                '<div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">'+
                'Billing Address</div><div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';
        if( this.masterNumberData !== null && this.masterNumberData !== undefined && this.masterNumberData.billingAddress !== null){
           content+=this.masterNumberData.billingAddress;
        }
        if( this.masterNumberData !== null && this.masterNumberData !== undefined && this.masterNumberData.billingAddressCity !== null){
           content+=" "+this.masterNumberData.billingAddressCity;
        }
        if( this.masterNumberData !== null && this.masterNumberData !== undefined && this.masterNumberData.billingAddressState !== null){
           content+=" "+this.masterNumberData.billingAddressState;
        }
        if( this.masterNumberData !== null && this.masterNumberData !== undefined && this.masterNumberData.billingAddressZip !== null){
           content+=" "+this.masterNumberData.billingAddressZip;
        }
        content+='</div></div></div><div class="em_row" style="width: 100%;padding: 0px;margin:5px 0px;float: left;overflow: hidden;">'+
                 '<div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;">'+
                 '<div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">'+
                 'Cause Of Loss</div><div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';                       
        if( this.masterNumberData !== null && this.masterNumberData !== undefined && this.masterNumberData.causeOfLoss !== null){
           content+=" "+this.masterNumberData.causeOfLoss;
        }
        content+='</div></div><div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;">'+
                 '<div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">'+
                 'Program Type</div><div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';
        if( this.masterNumberData !== null && this.masterNumberData !== undefined && this.masterNumberData.programType !== null){
           content+=" "+this.masterNumberData.programType;
        }
        content+='</div></div><div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;">'+
                 '<div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">Year Built'+
                 '</div><div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';
        if( this.masterNumberData !== null && this.masterNumberData !== undefined && this.masterNumberData.yearBuilt !== null){
           content+=" "+this.masterNumberData.yearBuilt;
        }
        content+='</div></div><div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;">'+
                 '<div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">'+
                 'Property Type</div><div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';
        if( this.masterNumberData !== null && this.masterNumberData !== undefined && this.masterNumberData.propertyType !== null){
           content+=" "+this.masterNumberData.propertyType;
        }         
        content+='</div></div></div></div><div class="em_title" style="width:100%;padding:5px 10px;margin:0px;float:left;font-size:16px;letter-spacing:0.5px;background:#595959;color:#FFF;box-shadow: 0 5px 5px -5px #000000;">'+
                 'Insurance Details</div><div class="em_pdf_info" style="width: 98.5%;float: left;padding:10px;margin: 0px;overflow: hidden;"><div class="em_row" style="width: 100%;padding: 0px;margin:5px 0px;float: left;overflow: hidden;">'+
                 '<div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;">'+
                 '<div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">Self Pay</div>'+
                 '<div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';
        if(this.masterNumberData !==null && this.masterNumberData !== undefined  && this.masterNumberData.isInsured == 'Y'){
          content+='No';
        }
        else{
          content+='Yes';
        }
        content+='</div></div><div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;">'+
                 '<div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">'+
                 'Claim No.</div><div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';
        if(this.insurances !==undefined && this.insurances !== null && this.insurances.claimNumber !== undefined && this.insurances.claimNumber !== null){
          content+=this.insurances.claimNumber;
        }
        content+='</div></div><div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;">'+
                 '<div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">'+
                 'Policy No.</div><div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';
        if(this.insurances !==undefined && this.insurances !== null && this.insurances.policyNumber !== undefined && this.insurances.policyNumber !== null){
          content+=this.insurances.policyNumber;
        }
        content+='</div></div><div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;">'+
                 '<div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">'+
                 'Insurance Company</div><div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';
        if(this.insurances !==undefined && this.insurances !== null && this.insurances.insuranceCompanyName !== undefined && this.insurances.insuranceCompanyName !== null){
          content+=this.insurances.insuranceCompanyName;
        }
        content+='</div></div></div><div class="em_row" style="width: 100%;padding: 0px;margin:5px 0px;float: left;overflow: hidden;">'+
                 '<div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;">'+
                 '<div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">'+
                 'Company Address</div><div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';
        if(this.insurances !==undefined && this.insurances !== null && this.insurances.address !== undefined && this.insurances.address !== null){
          content+=this.insurances.address;
        }
        content+='</div></div><div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;">'+
                 '<div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">'+
                 'Phone No.</div><div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';
        if(this.insurances !==undefined && this.insurances !== null && this.insurances.phone !== undefined && this.insurances.phone !== null){
          content+=this.insurances.phone;
        }
        content+='</div></div><div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;">'+
                 '<div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">'+
                 'Email</div><div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';
        if(this.insurances !==undefined && this.insurances !== null && this.insurances.email !== undefined && this.insurances.email !== null){
          content+=this.insurances.email;
        }
        content+='</div></div><div class="em_row_div" style="width: 25%;padding: 0px;margin:3px 0px;float: left;overflow: hidden;word-wrap: break-word;">'+
                 '<div class="em_text" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;font-size: 12px;color: #737373;letter-spacing: 0.5px;">'+
                 'Web Address</div><div class="em_value" style="width: 100%;padding: 0px;margin: 0px;float: left;color: #666666; overflow: hidden;font-size: 13px;font-weight: bold;word-wrap: break-word;">';
        if(this.insurances !==undefined && this.insurances !== null && this.insurances.website !== undefined && this.insurances.website !== null){
          content+=this.insurances.website;
        }
        content+='</div></div></div></div><div class="em_title" style="width:100%;padding:5px 10px;margin:0px;float:left;font-size:16px;letter-spacing:0.5px;background:#595959;color:#FFF;box-shadow: 0 5px 5px -5px #000000;">Description</div>'+
                  '<div class="em_pdf_info" style="width: 98.5%;float: left;padding:10px;margin: 0px;overflow: hidden;">'+
                  '<div class="desc_table" style="width: 100%;padding:0px;margin: 0px;float: left;overflow: hidden;">'+
                  '<table style="width: 100%;padding: 0px;margin: 0px;float: left;border-collapse: collapse;border: 1px solid #CCC;box-sizing: border-box;">'+
                  '<thead><tr><th style="background: #CCC;padding: 5px 10px;color: #595959;font-size: 13px;font-weight: bold;width: 10%;">Date</th>'+
                  '<th style="background: #CCC;padding: 5px 10px;color: #595959;font-size: 13px;font-weight: bold;width: 90%;text-align: left;">Description</th></tr></thead><tbody>';
        if(this.description !== null  && this.description !== undefined){
            for(let desc of this.description){
              content+='<tr><td style="padding: 3px 5px;border: 1px solid #CCC;box-sizing: border-box;font-size: 12px;letter-spacing: 0.5px;color: #595959;text-align: center;">'+
                        this.datePipe.transform(desc.updatedDatetime,'MM/dd/yyyy')+'</td> <td style="padding: 3px 5px;border: 1px solid #CCC;box-sizing: border-box;font-size: 12px;letter-spacing: 0.5px;color: #595959;text-align: left;">';
              if(desc.description !== null && desc.description !== undefined){
                  content+=desc.description;
              }
              content+='</td></tr>';
            }
        }
        content+='</tbody></table></div></div><div class="em_title" style="width:100%;padding:5px 10px;margin:0px;float:left;font-size:16px;letter-spacing:0.5px;background:#595959;color:#FFF;box-shadow: 0 5px 5px -5px #000000;">'+
                 'Contacts</div><div class="em_pdf_info" style="width: 98.5%;float: left;padding:10px;margin: 0px;overflow: hidden;"><div class="contact_table" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;">'+
                 '<table style="width: 100%;padding: 0px;margin: 0px;float: left;border-collapse: collapse;border: 1px solid #CCC;box-sizing: border-box;"><thead>'+
                 '<tr><th style="background: #CCC;padding: 5px 10px;color: #595959;font-size: 13px;font-weight: bold;">Company Name</th>'+
                 '<th style="background: #CCC;padding: 5px 10px;color: #595959;font-size: 13px;font-weight: bold;">First Name</th>'+
                 '<th style="background: #CCC;padding: 5px 10px;color: #595959;font-size: 13px;font-weight: bold;">Last Name</th>'+
                 '<th style="background: #CCC;padding: 5px 10px;color: #595959;font-size: 13px;font-weight: bold;">Email</th>'+
                 '<th style="background: #CCC;padding: 5px 10px;color: #595959;font-size: 13px;font-weight: bold;">Phone No.</th>'+
                 '<th style="background: #CCC;padding: 5px 10px;color: #595959;font-size: 13px;font-weight: bold;">Address</th>'+
                 '<th style="background: #CCC;padding: 5px 10px;color: #595959;font-size: 13px;font-weight: bold;">Contact Type</th></tr></thead><tbody>';
        if(this.contacts !== null  && this.contacts !== undefined){
            for(let contact of this.contacts){
              content+='<tr><td style="padding: 3px 5px;border: 1px solid #CCC;box-sizing: border-box;font-size: 12px;letter-spacing: 0.5px;color: #595959;">';
              if(contact.companyName !== null && contact.companyName !== undefined){
                  content+=contact.companyName;
              }
              content+='</td><td style="padding: 3px 5px;border: 1px solid #CCC;box-sizing: border-box;font-size: 12px;letter-spacing: 0.5px;color: #595959;">'+
                      contact.firstName+'</td><td style="padding: 3px 5px;border: 1px solid #CCC;box-sizing: border-box;font-size: 12px;letter-spacing: 0.5px;color: #595959;">'+
                      contact.lastName+'</td><td style="padding: 3px 5px;border: 1px solid #CCC;box-sizing: border-box;font-size: 12px;letter-spacing: 0.5px;color: #595959;">';
              if(contact.email){
                  content+=contact.email;
              }
              content+='</td><td style="padding: 3px 5px;border: 1px solid #CCC;box-sizing: border-box;font-size: 12px;letter-spacing: 0.5px;color: #595959;">';
              if(contact.phoneNo !== null && contact.phoneNo !== undefined){
                  content+=contact.phoneNo;
              }
              content+='</td><td style="padding: 3px 5px;border: 1px solid #CCC;box-sizing: border-box;font-size: 12px;letter-spacing: 0.5px;color: #595959;">';
              if(contact.address !== null && contact.address !== undefined){
                  content+=contact.phoneNo;
              }
              content+='</td><td style="padding: 3px 5px;border: 1px solid #CCC;box-sizing: border-box;font-size: 12px;letter-spacing: 0.5px;color: #595959;">'+contact.contactType+'</td></tr>';
            }
            content+='</tbody></table></div></div><div class="em_title" style="width:100%;padding:5px 10px;margin:0px;float:left;font-size:16px;letter-spacing:0.5px;background:#595959;color:#FFF;box-shadow: 0 5px 5px -5px #000000;">Files</div>'+
                    '<div class="em_pdf_info" style="width: 98.5%;float: left;padding:10px;margin: 0px;overflow: hidden;"><div class="contact_table" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;">'+
                    '<table style="width: 100%;padding: 0px;margin: 0px;float: left;border-collapse: collapse;border: 1px solid #CCC;box-sizing: border-box;"><thead><tr><th style="background: #CCC;padding: 5px 10px;color: #595959;font-size: 13px;font-weight: bold;">'+
                    'Current Status</th><th style="background: #CCC;padding: 5px 10px;color: #595959;font-size: 13px;font-weight: bold;">File No.</th>'+
                    '<th style="background: #CCC;padding: 5px 10px;color: #595959;font-size: 13px;font-weight: bold;">File Details</th>'+
                    '<th style="background: #CCC;padding: 5px 10px;color: #595959;font-size: 13px;font-weight: bold;">Supervisor</th>'+
                    '<th style="background: #CCC;padding: 5px 10px;color: #595959;font-size: 13px;font-weight: bold;">Estimator</th></tr></thead><tbody>';
            if(this.subJobs !== null && this.subJobs !== undefined){
                for(let subJob of this.subJobs){
                  content+='<tr><td style="padding: 3px 5px;border: 1px solid #CCC;box-sizing: border-box;font-size: 12px;letter-spacing: 0.5px;color: #595959;">';
                  if(subJob.status !== null && subJob.status !== undefined){
                    content+=subJob.status;
                  }
                  content+='</td><td style="padding: 3px 5px;border: 1px solid #CCC;box-sizing: border-box;font-size: 12px;letter-spacing: 0.5px;color: #595959;">'+
                           subJob.subJobCode+'</td><td style="padding: 3px 5px;border: 1px solid #CCC;box-sizing: border-box;font-size: 12px;letter-spacing: 0.5px;color: #595959;">'+
                           subJob.jobType+'</td><td style="padding: 3px 5px;border: 1px solid #CCC;box-sizing: border-box;font-size: 12px;letter-spacing: 0.5px;color: #595959;">';
                  if(subJob.supervisor !== null && subJob.supervisor !== undefined){
                    content+=subJob.supervisor;
                  }
                  content+='</td><td style="padding: 3px 5px;border: 1px solid #CCC;box-sizing: border-box;font-size: 12px;letter-spacing: 0.5px;color: #595959;">';
                  if(subJob.estimator !== null && subJob.estimator !== undefined){
                    content+=subJob.estimator;
                  }
                }
                content+='</td></tr>';
            }
            content+='</tbody></table></div></div><div class="em_title" style="width:100%;padding:5px 10px;margin:0px;float:left;font-size:16px;letter-spacing:0.5px;background:#595959;color:#FFF;box-shadow: 0 5px 5px -5px #000000;">'+
                     'Notes</div><div class="em_pdf_info" style="width: 98.5%;float: left;padding:10px;margin: 0px;overflow: hidden;"><div class="note_table" style="width: 100%;padding: 0px;margin: 0px;float: left;overflow: hidden;">'+
                     '<table style="width: 100%;padding: 0px;margin: 0px;float: left;border-collapse: collapse;border: 1px solid #CCC;box-sizing: border-box;"><thead><tr>'+
                     '<th style="background: #CCC;padding: 5px 10px;color: #595959;font-size: 13px;font-weight: bold;width: 15%;">Date</th>'+
                     '<th style="background: #CCC;padding: 5px 10px;color: #595959;font-size: 13px;font-weight: bold;width: 12%;text-align: left;">Note By</th>'+
                     '<th style="background: #CCC;padding: 5px 10px;color: #595959;font-size: 13px;font-weight: bold;width: 10%;text-align: center;">File Type</th>'+
                     '<th style="background: #CCC;padding: 5px 10px;color: #595959;font-size: 13px;font-weight: bold;width: 60%;text-align: left;">Notes Description</th></tr></thead><tbody>';
            if(this.notes !== null && this.notes !== undefined){
              for(let note of this.notes){
                content+='<tr><td style="padding: 3px 5px;border: 1px solid #CCC;box-sizing: border-box;font-size: 12px;letter-spacing: 0.5px;color: #595959;text-align: center;">'+
                          this.datePipe.transform(note.datetime,'MM/dd/yyyy')+'</td><td style="padding: 3px 5px;border: 1px solid #CCC;box-sizing: border-box;font-size: 12px;letter-spacing: 0.5px;color: #595959;">'+
                          note.noteby+'</td><td style="padding: 3px 5px;border: 1px solid #CCC;box-sizing: border-box;font-size: 12px;letter-spacing: 0.5px;color: #595959;text-align: center;">'+
                          note.jobType+'</td><td style="padding: 3px 5px;border: 1px solid #CCC;box-sizing: border-box;font-size: 12px;letter-spacing: 0.5px;color: #595959;">'+
                          note.note+'</td></tr>';
              }
            }
        content+='</tbody></table></div></div></div><div class="sign-div" style="width: 98%;float: left;padding: 0px;margin: 0px 1%;">'+
                 '<div class="sign-thanks" style="width: 100%; font-size: 16px;color: #666666;float: left;font-weight: bold;letter-spacing: 0.3px;">Thanks & Regards,</div>'+
                 '<div class="sign-name" style="width: 100%;font-size: 20px;color: #333333;float: left;font-weight: 600;letter-spacing: 0.3px;">Restoration Company.</div>'+
                 '</div><div class="discalim" style="width:98%;float: left;border-top:1px solid #333333;box-sizing: border-box;padding:0px;font-size: 12px;text-align: justify;color: #666666;margin:8px 1% 0px 1%;">'+
                 '"This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to whom they are addressed. If you have received this email in error please notify the system manager.'+
                 ' This message contains confidential information and is intended only for the individual named. If you are not the named addressee you should not disseminate, distribute or copy this e-mail. '+
                 'Please notify the sender immediately by e-mail if you have received this e-mail by mistake and delete this e-mail from your system. If you are not the intended recipient you are notified that disclosing, '+
                 'copying, distributing or taking any action in reliance on the contents of this information is strictly prohibited.”</div><div class="pwby" style="width:98%;margin:0px 1%;float:left;padding:0px 0px 5px 0px;text-align:right;overflow:hidden;color:#808080;">'+
                 'Powered by <b style="font-size:20px;color:#595959;">Xcelerate</b></div></body></html>';                  
        email.to=this.emailTo;
        email.subject=this.emailSubject;
        email.message=content;
        email.source=this.emailSource;
        email.sourceId=this.masterNumberData.mastNumId;
        email.mailOption=this.emailOption;
        this.url=APIUrls.hosturl+APIUrls.SendEmail;
      setTimeout(()=>{
       this.http.post(this.url, email)
        .subscribe(data=>{
          this.myData=data;
          this.showSuccess(this.myData.message);
          
        },
        (err: HttpErrorResponse) => {
            this.isError=true;
            this.serverErrorMessage = Messages.ServerErrorMessage;
            if (err.error instanceof Error) {
              console.log("Client-side error occured.",err);
            } else {
              console.log("Server-side error occured.",err);
            }
          }
        )},1000);
        this.displayEmail=false;
      }
      }
      else if(this.emailTo==undefined || this.emailTo==""){
        this.showMailToMsg=true;
      }
      else if(this.emailSubject==undefined || this.emailSubject==""){
        this.showSubjectMsg=true;
      }
    
    }

}