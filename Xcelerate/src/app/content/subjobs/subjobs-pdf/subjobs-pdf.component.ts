import { Component, Input } from  '@angular/core';
import { NotesInfo } from '../../../models/notes';
import { Description } from '../../../models/description';
import { RolesInfo } from '../../../models/roles';
import { ContactDetails } from '../../../models/contact-details';
import { InsuranceDetails } from '../../../models/insurance-details';

@Component({
  selector: 'subjob-pdf',
  templateUrl: './subjobs-pdf.component.html',
})
export class SubJobPDFComponent{
    @Input() fileDetails: any;
    insurances: InsuranceDetails;
    @Input() notes: NotesInfo[];
    @Input() description: Description[];
    @Input() roles: RolesInfo[];
    @Input() contacts: ContactDetails[];
    selfPay: string;

    @Input()
    set insurance(insurance: InsuranceDetails){
        this.insurances=insurance;
        if(this.insurances!=null && this.insurances!=undefined){
        this.selfPay="No";
        }
        else{
        this.selfPay="Yes";
        }
    }
}